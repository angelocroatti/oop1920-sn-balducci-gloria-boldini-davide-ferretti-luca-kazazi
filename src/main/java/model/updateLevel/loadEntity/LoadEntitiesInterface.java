package model.updateLevel.loadEntity;

public interface LoadEntitiesInterface {

    /**
     * method to load all entities of a given level.
     */
    void createEntities();

}
