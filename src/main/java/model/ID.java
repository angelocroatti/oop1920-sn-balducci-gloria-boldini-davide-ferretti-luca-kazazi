package model;

public enum ID {
    /**
     * ID of all the objects in the game.
     */
    PLAYER, RAY, DOOR, GAINTIMEPU, INVISIBLEPU, KNIFEPU, LIFEUPPU, SPEEDUPPU, WALL, FIREDB, FREEZEDB, SLOWDOWNDB,
    TIMEDOWNDB, ENEMY, PORTAL, LAVA;
}
